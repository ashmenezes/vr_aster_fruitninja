﻿using BzKovSoft.ObjectSlicer;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Profiling;

namespace BzKovSoft.ObjectSlicerSamples
{
    public class FleshMaterial : BzSliceableObjectBase
    {
        protected override void OnSliceFinished(BzSliceTryResult result)
        {
            if (!result.sliced)
                return;

            // get pieces
            GameObject[] pieces = new GameObject[2];
            pieces[0] = result.outObjectNeg;
            pieces[1] = result.outObjectPos;

            pieces[0].GetComponent<MeshCollider>().isTrigger = true;
            pieces[1].GetComponent<MeshCollider>().isTrigger = true;

            if (pieces[0].name.StartsWith("hand_1:hand_1:pasted__polySurface2"))
            {
                cigarette(pieces);
            }
            else if (pieces[0].name.StartsWith("pasted__polySurface1"))
            {
                caffeine(pieces);
            }
            else if (pieces[0].name.StartsWith("pasted__polySurface2"))
            {
                sugar(pieces);
            }
            else if (pieces[0].name.StartsWith("pasted__polySurface3"))
            {
                junkfood(pieces);
            }
            else if (pieces[0].name.StartsWith("pasted__polySurface4"))
            {
                nailbiting(pieces);
            }
            else if (pieces[0].name.StartsWith("hand_1:pasted__polySurface5"))
            {
                stagefear(pieces);
            }
            else if (pieces[0].name.StartsWith("cdbody"))
            {
                carbonatedDrinks(pieces);
            }
            else if (pieces[0].name.StartsWith("isbody"))
            {
                inadequatesleep(pieces);
            }
            else if (pieces[0].name.StartsWith("lbody"))
            {
                laziness(pieces);
            }
            else if (pieces[0].name.StartsWith("nebody"))
            {
                noexercise(pieces);
            }
            else if (pieces[0].name.StartsWith("nftbody"))
            {
                nofamilytime(pieces);
            }
            else if (pieces[0].name.StartsWith("ppbody"))
            {
                poorposture(pieces);
            }
            else if (pieces[0].name.StartsWith("pbody"))
            {
                procrastination(pieces);
            }
            else if (pieces[0].name.StartsWith("smbody"))
            {
                socialmedia(pieces);
            }
            else if (pieces[0].name.StartsWith("sbody"))
            {
                stress(pieces);
            }
            else if (pieces[0].name.StartsWith("blueberry_2"))
            {
                blueberry(pieces);
            }
            else if (pieces[0].name.StartsWith("omega_complete_2"))
            {
                omegacomplete(pieces);
            }
            else if (pieces[0].name.StartsWith("purafitorange"))
            {
                orange(pieces);
            }
            else if (pieces[0].name.StartsWith("purafitgreen"))
            {
                green(pieces);
            }
            else if (pieces[0].name.StartsWith("realfood"))
            {
                realfood(pieces);
            }

        }

        #region vitamin specific methods

        public void blueberry(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.sliceVitaminClip, 0.5f); // Play sword swing audio
            FruitSpawner.instance.Heal(FruitSpawner.instance.vitaminPanels[0]);

            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            p.GetComponent<BoxCollider>().enabled = false;

            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void omegacomplete(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.sliceVitaminClip, 0.5f); // Play sword swing audio
            FruitSpawner.instance.Heal(FruitSpawner.instance.vitaminPanels[1]);

            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            p.GetComponent<BoxCollider>().enabled = false;

            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void orange(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.sliceVitaminClip, 0.5f); // Play sword swing audio
            FruitSpawner.instance.Heal(FruitSpawner.instance.vitaminPanels[3]);

            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            p.GetComponent<BoxCollider>().enabled = false;

            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void green(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.sliceVitaminClip, 0.5f); // Play sword swing audio
            FruitSpawner.instance.Heal(FruitSpawner.instance.vitaminPanels[2]);

            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            p.GetComponent<BoxCollider>().enabled = false;

            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void realfood(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.sliceVitaminClip, 0.5f); // Play sword swing audio
            FruitSpawner.instance.Heal(FruitSpawner.instance.vitaminPanels[4]);

            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            p.GetComponent<BoxCollider>().enabled = false;

            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        #endregion

        #region enemy specific methods

        public void cigarette(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.parent.parent.gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            p.transform.parent.SetParent(GameObject.Find("Pool").transform);

            Transform par = pieces[0].transform.parent.parent.parent.parent;
            //p.gameObject.AddComponent<Rigidbody>();
            par.GetComponent<Rigidbody>().useGravity = true;
            par.GetComponent<Rigidbody>().isKinematic = false;
            par.GetComponent<Fruit>().enabled = false;

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void caffeine(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void sugar(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void junkfood(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(2).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void nailbiting(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void stagefear(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void carbonatedDrinks(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void inadequatesleep(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void laziness(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.parent.GetChild(2).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void noexercise(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void nofamilytime(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(2).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void poorposture(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }

        public void procrastination(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(3).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }
        
        public void socialmedia(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }
        
        public void stress(GameObject[] pieces)
        {
            FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f); // Play sword swing audio
            // remove other parts of victim from view
            GameObject p = pieces[0].transform.parent.GetChild(3).gameObject;
            foreach (Transform child in p.transform)
            {
                if (child.GetComponent<MeshCollider>() != null)
                {
                    child.GetComponent<MeshCollider>().enabled = false;
                }
            }
            pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

            // Add gravity to pieces
            foreach (GameObject g in pieces)
            {
                Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
                r.useGravity = true;
                r.isKinematic = false;
            }
        }
        #endregion

        protected override BzSliceTryData PrepareData(Plane plane)
        {
            ResultData addData = new ResultData();

            // count vertices
            var filters = GetComponentsInChildren<MeshFilter>();
            for (int i = 0; i < filters.Length; i++)
            {
                addData.vertexCount += filters[i].sharedMesh.vertexCount;
            }

            // remember start time
            addData.stopwatch = Stopwatch.StartNew();

            // colliders that will be participating in slicing
            var colliders = gameObject.GetComponentsInChildren<Collider>();

            // return data
            return new BzSliceTryData()
            {
                // componentManager: this class will manage components on sliced objects
                componentManager = new StaticComponentManager(gameObject, plane, colliders),
                plane = plane,
                addData = addData,
            };
        }
    }

    class ResultData
    {
        public int vertexCount;
        public Stopwatch stopwatch;
    }
}
