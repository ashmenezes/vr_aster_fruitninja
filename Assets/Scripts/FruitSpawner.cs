﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class FruitSpawner : MonoBehaviour {

    [Header("Variables")]
    public bool chooseCaffeine = false;
    public bool chooseCigarette = false;
    public bool chooseJunkfood = false;
    public bool chooseNailbiting = false;
    public bool chooseStagefear = false;
    public bool chooseSugar = false;
    public bool chooseStress = false;
    public bool chooseSocialmedia = false;
    public bool chooseProcrastination = false;
    public bool choosePoorposture = false;
    public bool chooseNofamilytime = false;
    public bool chooseNoexercise = false;
    public bool chooseLaziness = false;
    public bool chooseInadecuatesleep = false;
    public bool chooseCarbonateddrinks = false;

    int chooseCount = 0;
    int maxChooseCount = 4;

    public enum GameState { Start, Play, Dead, GameOver };
    public GameState currState = GameState.Start;

    bool spawnVitamin = false;

    bool isCountdownStarted = false;
    float countdown = 0f;
    int prev = 5;

    int health;
    int maxhealth = 100;
    public float timer = 0f;
    bool getFirstVita = false;
    bool GodMode = false;
    bool InfiniteTime = false;

    [Header("References")]
    public SimpleHealthBar VRhealthBar;
    public GameObject StartCanvas;
    public GameObject StartPanel;
    public GameObject healthPanel;
    public GameObject vitaPanel;
    public GameObject ChoosePage;
    public GameObject IntroPage;
    public GameObject IntroPage1;
    public GameObject DamagePanel;
    public GameObject WinPanel;
    public GameObject WinPanel1;
    public GameObject CountDownPanel;
    public GameObject ShowVitaminsPanel;

    public GameObject SpawnContainer;

    public GameObject[] vitaminPanels;

    public GameObject[] listOfPrefabs;
    public List<GameObject> fruitPrefab;
    public GameObject[] vitaminPrefabs;

    public AudioClip SwordSwing;
    public AudioClip sliceVitaminClip;
    public AudioClip IntroductionClip;
    public AudioClip SelectionClip;
    public AudioClip GetReadyClip;
    public AudioClip EnergyBoostersClip;
    public AudioClip LetsGetStartedClip;
    public AudioClip ReloadClip;
    public AudioClip CongratulationsClip;

    public AudioClip introMusic;
    public AudioClip fightMusic;
    public AudioClip gameoverMusic;
    public AudioClip countClip;
    public AudioClip GoClip;

    public AudioClip[] zombieSounds;

    public GameObject OneImage;
    public GameObject TwoImage;
    public GameObject ThreeImage;
    public GameObject LetsGoImage;

    public AudioSource MusicPlayer;

    public static FruitSpawner instance;

    private void Awake()
    {
        instance = this;
    }

    // Use this for initialization
    void Start() {
        health = maxhealth;
        SpawnContainer = GameObject.Find("Spawn");
        //Init();   
    }

    public IEnumerator GoToIntroPage()
    {
        yield return new WaitForSeconds(1f);
        MusicPlayer.PlayOneShot(introMusic, 0.05f);
        IntroPage.SetActive(true);
        StartPanel.SetActive(false);

        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(IntroductionClip, 1f);
        Invoke("showIntroPage1", 4f);
        Invoke("GoToChoosePage", IntroductionClip.length);
    }

    public void showIntroPage1()
    {
        IntroPage1.SetActive(true);
        IntroPage.SetActive(false);
    }

    public void GoToChoosePage()
    {
        IntroPage1.SetActive(false);
        ChoosePage.SetActive(true);
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(SelectionClip, 1f);
    }

    public IEnumerator StartGame()
    {
        MusicPlayer.PlayOneShot(fightMusic, 0.1f);

        yield return new WaitForSeconds(1f);

        StartCanvas.SetActive(false);

        if (chooseCaffeine) fruitPrefab.Add(listOfPrefabs[0]);
        if (chooseCigarette) fruitPrefab.Add(listOfPrefabs[1]);
        if (chooseJunkfood) fruitPrefab.Add(listOfPrefabs[2]);
        if (chooseNailbiting) fruitPrefab.Add(listOfPrefabs[3]);
        if (chooseStagefear) fruitPrefab.Add(listOfPrefabs[4]);
        if (chooseSugar) fruitPrefab.Add(listOfPrefabs[5]);
        if (chooseStress) fruitPrefab.Add(listOfPrefabs[6]);
        if (chooseSocialmedia) fruitPrefab.Add(listOfPrefabs[7]);
        if (chooseProcrastination) fruitPrefab.Add(listOfPrefabs[8]);
        if (choosePoorposture) fruitPrefab.Add(listOfPrefabs[9]);
        if (chooseNofamilytime) fruitPrefab.Add(listOfPrefabs[10]);
        if (chooseNoexercise) fruitPrefab.Add(listOfPrefabs[11]);
        if (chooseLaziness) fruitPrefab.Add(listOfPrefabs[12]);
        if (chooseInadecuatesleep) fruitPrefab.Add(listOfPrefabs[13]);
        if (chooseCarbonateddrinks) fruitPrefab.Add(listOfPrefabs[14]);

        StartCoroutine(Init());
    }

    public void IncrementChooseCount()
    {
        chooseCount = chooseCount < 5 ? chooseCount + 1 : chooseCount;
        if (chooseCount == 5)
        {
            ChoosePage.SetActive(false);
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().PlayOneShot(GetReadyClip, 1f);
            StartCoroutine(PlayEnergyBoostersClip());
        }
    }

    public IEnumerator PlayEnergyBoostersClip()
    {
        yield return new WaitForSeconds(GetReadyClip.length);
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(EnergyBoostersClip, 1f);
        DisplayVitaminsPanel();
        StartCoroutine(PlayLetsGetStartedClip());
    }

    public IEnumerator PlayLetsGetStartedClip()
    {
        yield return new WaitForSeconds(EnergyBoostersClip.length + 1f);
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(LetsGetStartedClip, 1f);
        ShowVitaminsPanel.SetActive(false);
        yield return new WaitForSeconds(LetsGetStartedClip.length-3.5f);
        Invoke("StartCountdown", LetsGetStartedClip.length);
    }

    public void DisplayVitaminsPanel()
    {
        ShowVitaminsPanel.SetActive(true);
    }

    public void StartCountdown()
    {
        ChoosePage.SetActive(false);
        CountDownPanel.SetActive(true);
        isCountdownStarted = true;
    }

    public IEnumerator Init()
    {
        if (currState == GameState.Start)
        {
            yield return new WaitForSeconds(2f);
            StartCoroutine(SpawnFruit());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            InfiniteTime = !InfiniteTime;
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene("AsterVRCity");
        }
        else if (Input.GetKeyDown(KeyCode.G))
        {
            GodMode = !GodMode;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            Init();
        }

        if (currState == GameState.Play)
        {
            timer += Time.deltaTime;
            if (Mathf.RoundToInt(timer) == 30f)
                spawnVitamin = true;
            if (timer > 60f && !InfiniteTime)
                YouWin();
        }

        if (isCountdownStarted)
        {
            countdown += Time.deltaTime;
            float number = 5 - Mathf.Round(countdown);

            if (number == 4)
            {
                LetsGoImage.SetActive(false);
                ThreeImage.SetActive(true);
                TwoImage.SetActive(false);
                OneImage.SetActive(false);
                if (prev == 5)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(countClip, 0.1f);
                    prev = 4;
                }
            }
            else if (number == 3)
            {
                LetsGoImage.SetActive(false);
                ThreeImage.SetActive(false);
                TwoImage.SetActive(true);
                OneImage.SetActive(false);
                if (prev == 4)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(countClip, 0.1f);
                    prev = 3;
                }
            }
            else if (number == 2)
            {
                LetsGoImage.SetActive(false);
                ThreeImage.SetActive(false);
                TwoImage.SetActive(false);
                OneImage.SetActive(true);
                if (prev == 3)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(countClip, 0.1f);
                    prev = 2;
                }
            }
            else if (number == 1)
            {
                LetsGoImage.SetActive(true);
                ThreeImage.SetActive(false);
                TwoImage.SetActive(false);
                OneImage.SetActive(false);
                if (prev == 2)
                {
                    GetComponent<AudioSource>().Stop();
                    GetComponent<AudioSource>().PlayOneShot(GoClip, 0.1f);
                    prev = 1;
                }
            }

            if (countdown >= 5)
            {
                isCountdownStarted = false;
                currState = GameState.Start;
                StartCoroutine(StartGame());
            }
        }
    }

    IEnumerator SpawnFruit()
    {
        currState = GameState.Play;

        while (currState == GameState.Play)
        {

            if (spawnVitamin)  // if vitamin
            {
                GameObject go = Instantiate(vitaminPrefabs[Random.Range(0, vitaminPrefabs.Length)]) as GameObject;  //  spawn a randomly chosen vitamin
                go.transform.SetParent(SpawnContainer.transform);

                Vector3 center = transform.position;

                Vector3 pos = RandomCircle(center, 5.0f);
                Quaternion rot = Quaternion.FromToRotation(Vector3.forward, center - pos);

                go.transform.position = new Vector3(pos.x, pos.y + 0.75f /*+ 0.75f*/, pos.z);
                //go.transform.rotation = new Quaternion(rot.x, go.transform.rotation.y, rot.z, rot.w);
                go.transform.rotation = rot;

                spawnVitamin = false;

                yield return new WaitForSeconds(1.5f - (timer / 60));
            }
            else // if enemy
            {
                int rand = Random.Range(0, fruitPrefab.Count);
                GameObject go = Instantiate(fruitPrefab[rand]) as GameObject;
                go.transform.SetParent(SpawnContainer.transform);

                Vector3 center = transform.position;

                Vector3 pos = RandomCircle(center, 5.0f);
                Quaternion rot = Quaternion.FromToRotation(Vector3.forward, center - pos);

                go.transform.position = new Vector3(pos.x, pos.y + 0.75f /*+ 0.75f*/, pos.z);
                //go.transform.rotation = new Quaternion(rot.x, go.transform.rotation.y, rot.z, rot.w);
                go.transform.rotation = rot;

                yield return new WaitForSeconds(1.5f - (timer / 60));
            }
        }
    }

    Vector3 RandomCircle(Vector3 center, float radius)
    {
        //float ang = Random.value * 360;
        float ang = Random.Range(-80f, 80f); // 70

        Vector3 pos;
        pos.z = center.z + radius * Mathf.Sin(ang * Mathf.Deg2Rad);
        pos.x = center.x - radius * Mathf.Cos(ang * Mathf.Deg2Rad);
        pos.y = center.y;
        return pos;
    }

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "Fruit" && !collider.name.EndsWith("pos") && !collider.name.EndsWith("neg"))
        {
            Destroy(collider.GetComponent<GetParent>().parent.gameObject);
            if (!GodMode) StartCoroutine(Damage());

            if (health == 0)
            {
                YouWin();
            }
        }
        else if (collider.gameObject.tag == "Vitamin")
        {
            Destroy(collider.gameObject);
        }
    }

    public void YouWin()
    {
        MusicPlayer.PlayOneShot(gameoverMusic, 0.02f);
        WinPanel.SetActive(true);
        currState = GameState.GameOver;

        foreach (Transform child in SpawnContainer.transform)
        {
            Destroy(child.gameObject);
        }
        GetComponent<AudioSource>().Stop();
        GetComponent<AudioSource>().PlayOneShot(CongratulationsClip, 1f);
        Invoke("YouWin1", 5f);
        Invoke("Restart", gameoverMusic.length + 1f);
    }

    public void YouWin1()
    {
        WinPanel1.SetActive(true);
        WinPanel.SetActive(false);
    }

    public IEnumerator Damage()
    {
        health -= 20;
        VRhealthBar.UpdateBar(health, maxhealth);
        DamagePanel.SetActive(true);

        if (health == 40)
        {
            GetComponent<AudioSource>().Stop();
            GetComponent<AudioSource>().PlayOneShot(ReloadClip, 1f);
            spawnVitamin = true;
        }

        yield return new WaitForSeconds(0.5f);
        DamagePanel.SetActive(false);
    }

    public void Heal(GameObject panel)
    {
        StartCoroutine(GetFirstVita(panel));
    }

    public IEnumerator GetFirstVita(GameObject panel)
    {
        currState = GameState.Dead;
        panel.SetActive(true);
        getFirstVita = true;

        /*yield return new WaitForSeconds(2);
        health += 20;
        Debug.Log(health);
        healthBar.UpdateBar(health, maxhealth);*/

        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);
        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);
        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);
        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);
        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);
        health += 5;
        VRhealthBar.UpdateBar(health, maxhealth);
        yield return new WaitForSeconds(1);

        if (health > maxhealth) health = maxhealth;

        panel.SetActive(false);
        currState = GameState.Play;

        StartCoroutine(SpawnFruit());
    }

    public void Restart()
    {
        SceneManager.LoadScene("AsterVRCity");
    }

}