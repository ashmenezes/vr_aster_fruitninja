﻿using UnityEngine;
using System.Collections;
using Valve.VR.InteractionSystem;
using Valve.VR;
using Valve.VR.InteractionSystem.Sample;
using UnityEngine.UI;

[RequireComponent (typeof(Rigidbody))]
public class SwordCutter : MonoBehaviour {

    //public SteamVR_TrackedController controller;

    public SteamVR_Action_Vibration hapticAction;
    public SteamVR_Action_Boolean grabPinch; //Grab Pinch is the trigger, select from inspecter
    public SteamVR_Input_Sources inputSource = SteamVR_Input_Sources.Any;//which controller

    public GameObject sword;

    public Material capMaterial;
    public AudioClip clip;
    Transform pool;

    float timer = 0f;

    public bool inButton = false;

    public enum trigger { Start, Restart };
    public trigger curr;

    private void Start()
    {
        pool = GameObject.Find("Pool").transform;
        GetComponent<AudioSource>().clip = clip;
    }

    private void Update()
    {
        timer += Time.deltaTime;

        if (timer > 3f)
        {
            foreach (Transform child in pool)
            {
              Destroy(child.gameObject);
            }
            timer = 0f;
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        GameObject victim = collision.collider.gameObject;

        if (victim.tag == "Fruit")
        {
            StartCoroutine(Slice(victim));
        }
        else if (victim.tag == "Vitamin")
        {
            StartCoroutine(Slice(victim));
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        GameObject victim = collider.gameObject;

        if (victim.tag == "Fruit")
        {
            StartCoroutine(Slice(victim));
        }
        else if (victim.tag == "Vitamin")
        {
            StartCoroutine(Slice(victim));
        }
    }

    IEnumerator Slice(GameObject victim)
    {
        //victim.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.None;

        /*if (victim.tag.StartsWith("Fruit"))
        {
            Transform p = victim.GetComponent<GetParent>().parent;
            p.SetParent(pool);
            //p.gameObject.AddComponent<Rigidbody>();
            p.gameObject.GetComponent<Rigidbody>().useGravity = true;
            p.gameObject.GetComponent<Rigidbody>().isKinematic = false;
            p.GetComponent<Fruit>().enabled = false;
        }*/

        // Vibrate
        hapticAction.Execute(0, 1, 50, 3500, GetComponent<Hand>().handType);

        yield return null;
    }

}
