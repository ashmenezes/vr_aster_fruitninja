﻿using BzKovSoft.ObjectSlicer;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class SliceButton : BzSliceableObjectBase {

    protected override void OnSliceFinished(BzSliceTryResult result)
    {
        if (!result.sliced)
            return;

        // Play sword swing audio
        FruitSpawner.instance.GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.SwordSwing, 0.5f);

        // get pieces
        GameObject[] pieces = new GameObject[2];
        pieces[0] = result.outObjectNeg;
        pieces[1] = result.outObjectPos;

        pieces[0].GetComponent<MeshCollider>().enabled = false;
        pieces[1].GetComponent<MeshCollider>().enabled = false;

        string name = pieces[0].GetComponent<GetParent>().parent.name.Split('_')[0];

        GameObject parent = pieces[0].GetComponent<GetParent>().parent.gameObject;

        pieces[0].GetComponent<MeshCollider>().enabled = false;
        pieces[1].GetComponent<MeshCollider>().enabled = false;

        Rigidbody rb = parent.GetComponent<Rigidbody>() == null ? parent.AddComponent<Rigidbody>() : parent.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        rb.useGravity = true;

        switch (name)
        {
            case "caffeine":
                caffeine(pieces);
                FruitSpawner.instance.chooseCaffeine = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "sugar":
                sugar(pieces);
                FruitSpawner.instance.chooseSugar = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "cigarette":
                cigarette(pieces);
                FruitSpawner.instance.chooseCigarette = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "junkfood":
                junkfood(pieces);
                FruitSpawner.instance.chooseJunkfood = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "nailbiting":
                nailbiting(pieces);
                FruitSpawner.instance.chooseNailbiting = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "stagefear":
                stagefear(pieces);
                FruitSpawner.instance.chooseStagefear = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "stress":
                stress(pieces);
                FruitSpawner.instance.chooseStress = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "socialmedia":
                socialmedia(pieces);
                FruitSpawner.instance.chooseSocialmedia = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "procrastination":
                procrastination(pieces);
                FruitSpawner.instance.chooseProcrastination = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "poorposture":
                poorposture(pieces);
                FruitSpawner.instance.choosePoorposture = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "nofamilytime":
                nofamilytime(pieces);
                FruitSpawner.instance.chooseNofamilytime = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "noexercise":
                noexercise(pieces);
                FruitSpawner.instance.chooseNoexercise = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "laziness":
                laziness(pieces);
                FruitSpawner.instance.chooseLaziness = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "inadecuatesleep":
                inadequatesleep(pieces);
                FruitSpawner.instance.chooseInadecuatesleep = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "carbonatedDrinks":
                carbonatedDrinks(pieces);
                FruitSpawner.instance.chooseCarbonateddrinks = true;
                FruitSpawner.instance.IncrementChooseCount();
                break;
            case "Start":
                StartCoroutine(FruitSpawner.instance.GoToIntroPage());
                break;
        }

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    #region object specific methods
    public void cigarette(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.parent.parent.gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        p.transform.parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void caffeine(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.AddComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void sugar(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void junkfood(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(2).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void nailbiting(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void stagefear(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }


    public void carbonatedDrinks(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void inadequatesleep(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void laziness(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.parent.GetChild(2).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void noexercise(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void nofamilytime(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(2).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void poorposture(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void procrastination(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(3).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void socialmedia(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(1).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    public void stress(GameObject[] pieces)
    {

        // remove other parts of victim from view
        GameObject p = pieces[0].transform.parent.GetChild(3).gameObject;
        foreach (Transform child in p.transform)
        {
            if (child.GetComponent<MeshCollider>() != null)
            {
                child.GetComponent<MeshCollider>().enabled = false;
            }
        }
        pieces[0].GetComponent<GetParent>().parent.SetParent(GameObject.Find("Pool").transform);

        // Add gravity to pieces
        foreach (GameObject g in pieces)
        {
            Rigidbody r = g.GetComponent<Rigidbody>() == null ? g.AddComponent<Rigidbody>() : g.GetComponent<Rigidbody>();
            r.useGravity = true;
            r.isKinematic = false;
        }
    }

    #endregion

    protected override BzSliceTryData PrepareData(Plane plane)
    {
        ResultData addData = new ResultData();

        // count vertices
        var filters = GetComponentsInChildren<MeshFilter>();
        for (int i = 0; i < filters.Length; i++)
        {
            addData.vertexCount += filters[i].sharedMesh.vertexCount;
        }

        // remember start time
        addData.stopwatch = Stopwatch.StartNew();

        // colliders that will be participating in slicing
        var colliders = gameObject.GetComponentsInChildren<Collider>();

        // return data
        return new BzSliceTryData()
        {
            // componentManager: this class will manage components on sliced objects
            componentManager = new StaticComponentManager(gameObject, plane, colliders),
            plane = plane,
            addData = addData,
        };
    }

    class ResultData
    {
        public int vertexCount;
        public Stopwatch stopwatch;
    }
}
