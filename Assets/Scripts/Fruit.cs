﻿using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Fruit : MonoBehaviour {

    Vector3 target;

    float speed = 1f;

    private void Start()
    {
        target = GameObject.Find("Player").transform.position;
    }

    // Update is called once per frame
    void Update () {
        if (FruitSpawner.instance.currState == FruitSpawner.GameState.Play)
        {
            Vector3 newpos = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
            transform.position = new Vector3(newpos.x, transform.position.y, newpos.z);

            int rand = Random.Range(0, 5000);
            if (rand > 4990)
            {
                GetComponent<AudioSource>().PlayOneShot(FruitSpawner.instance.zombieSounds[rand - 4991], 0.1f);
            }
        }
        else if (FruitSpawner.instance.currState == FruitSpawner.GameState.GameOver)
        {
            //GetComponent<Rigidbody>().isKinematic = true;
        }
    }
}
